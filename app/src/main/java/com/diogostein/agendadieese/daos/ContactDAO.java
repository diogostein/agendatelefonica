package com.diogostein.agendadieese.daos;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.diogostein.agendadieese.database.DBHelper;
import com.diogostein.agendadieese.helpers.DateHelper;
import com.diogostein.agendadieese.models.ContactModel;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Diogo on 08/05/2015.
 */
public class ContactDAO {

    private SQLiteDatabase mDatabase;
    private DBHelper mDBHelper;

    public ContactDAO(Context context) {
        mDBHelper = new DBHelper(context);
    }

    public void open() throws SQLException {
        mDatabase = mDBHelper.getWritableDatabase();
    }

    public void close() {
        mDBHelper.close();
    }

    public long create(ContactModel contact) {
        ContentValues cv = new ContentValues();
        cv.put(DBHelper.COLUMN_NAME, contact.name);
        cv.put(DBHelper.COLUMN_BIRTH, contact.birth);
        cv.put(DBHelper.COLUMN_COMPANY, contact.company);
        cv.put(DBHelper.COLUMN_EMAIL, contact.email);
        cv.put(DBHelper.COLUMN_PHONE_CEL, contact.phoneCel);
        cv.put(DBHelper.COLUMN_PHONE_RES, contact.phoneRes);
        cv.put(DBHelper.COLUMN_PHONE_COM, contact.phoneCom);
        cv.put(DBHelper.COLUMN_CREATED_AT, DateHelper.getCurrentDate());
        cv.put(DBHelper.COLUMN_UPDATED_AT, DateHelper.getCurrentDate());
        return mDatabase.insert(DBHelper.TABLE_CONTACTS, null, cv);
    }

    public int update(ContactModel contact) {
        ContentValues cv = new ContentValues();
        cv.put(DBHelper.COLUMN_NAME, contact.name);
        cv.put(DBHelper.COLUMN_BIRTH, contact.birth);
        cv.put(DBHelper.COLUMN_COMPANY, contact.company);
        cv.put(DBHelper.COLUMN_EMAIL, contact.email);
        cv.put(DBHelper.COLUMN_PHONE_CEL, contact.phoneCel);
        cv.put(DBHelper.COLUMN_PHONE_RES, contact.phoneRes);
        cv.put(DBHelper.COLUMN_PHONE_COM, contact.phoneCom);
        cv.put(DBHelper.COLUMN_UPDATED_AT, DateHelper.getCurrentDate());
        return mDatabase.update(
                DBHelper.TABLE_CONTACTS,
                cv,
                DBHelper.COLUMN_ID + " = ?",
                new String[] {String.valueOf(contact.id)});
    }

    public boolean delete(long idContact) {
        int rowsAffected = mDatabase.delete(
                DBHelper.TABLE_CONTACTS,
                DBHelper.COLUMN_ID + " = ?",
                new String[] {String.valueOf(idContact)});
        return rowsAffected > 0;
    }

    public boolean groupDelete(String[] ids) {
        int rowsAffected = mDatabase.delete(
                DBHelper.TABLE_CONTACTS,
                DBHelper.COLUMN_ID + " IN (" + new String(new char[ids.length-1]).replace("\0", "?,") + "?)",
                ids);
        return rowsAffected > 0;
    }

    public List<ContactModel> getAllContacts() {
        List<ContactModel> contacts = new ArrayList<>();
        Cursor cursor = mDatabase.query(
                DBHelper.TABLE_CONTACTS,
                new String[] {
                        DBHelper.COLUMN_ID,
                        DBHelper.COLUMN_NAME,
                        DBHelper.COLUMN_BIRTH,
                        DBHelper.COLUMN_COMPANY,
                        DBHelper.COLUMN_EMAIL,
                        DBHelper.COLUMN_PHONE_CEL,
                        DBHelper.COLUMN_PHONE_RES,
                        DBHelper.COLUMN_PHONE_COM,
                        DBHelper.COLUMN_CREATED_AT,
                        DBHelper.COLUMN_UPDATED_AT,
                },
                null, null, null, null, null);

        cursor.moveToFirst();
        while(!cursor.isAfterLast()) {
            contacts.add(cursorToContact(cursor));
            cursor.moveToNext();
        }
        cursor.close();
        return contacts;
    }

    private ContactModel cursorToContact(Cursor cursor) {
        return new ContactModel(
                cursor.getLong(0),
                cursor.getString(1),
                cursor.getString(2),
                cursor.getString(3),
                cursor.getString(4),
                cursor.getString(5),
                cursor.getString(6),
                cursor.getString(7),
                cursor.getString(8),
                cursor.getString(9)
        );
    }

}
