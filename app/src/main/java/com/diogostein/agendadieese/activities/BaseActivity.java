package com.diogostein.agendadieese.activities;

import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ListView;

import com.diogostein.agendadieese.R;
import com.diogostein.agendadieese.adapters.DrawerMenuAdapter;
import com.diogostein.agendadieese.dialogs.AboutDialog;
import com.diogostein.agendadieese.models.DrawerItemModel;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Diogo on 08/05/2015.
 */
public class BaseActivity extends ActionBarActivity implements DrawerMenuAdapter.OnDrawerItemClickListener {

    private static final int NAV_ITEM_ABOUT = 1;

    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private Toolbar mToolbar;

    private List<DrawerItemModel> mDrawerItems;

    private ListView mDrawerList;

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        setUpNavigationDrawer();
        setUpDrawerMenu();
    }

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
        if (mToolbar == null){
            mToolbar = (Toolbar) findViewById(R.id.toolbar);
            if(mToolbar != null){
                setSupportActionBar(mToolbar);
                getSupportActionBar().setDisplayShowHomeEnabled(true);
            }
        }
    }

    @Override
    public void onDrawerItemClick(DrawerItemModel drawerItem) {
        switch(drawerItem.id) {
            case NAV_ITEM_ABOUT: AboutDialog.showDialog(this); break;
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    private void setUpNavigationDrawer() {
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, mToolbar, 0, 0) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                supportInvalidateOptionsMenu();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                supportInvalidateOptionsMenu();
            }
        };

        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();
    }

    private void setUpDrawerMenu() {
        mDrawerList = (ListView) findViewById(R.id.drawer_list);
        int[] navIds = getResources().getIntArray(R.array.nav_ids);
        String[] navTitles = getResources().getStringArray(R.array.nav_titles);
        TypedArray navIcons = getResources().obtainTypedArray(R.array.nav_icons);
        mDrawerItems = new ArrayList<>();
        for(int i = 0; i < navIds.length; i++) {
            mDrawerItems.add(new DrawerItemModel(navIds[i], navTitles[i], navIcons.getDrawable(i)));
        }
        DrawerMenuAdapter adapter = new DrawerMenuAdapter(this, R.layout.adapter_navdrawer, mDrawerItems, this);
        mDrawerList.setAdapter(adapter);
    }
}
