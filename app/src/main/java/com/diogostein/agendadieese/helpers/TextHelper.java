package com.diogostein.agendadieese.helpers;

import android.util.Patterns;

/**
 * Created by Diogo on 11/05/2015.
 */
public final class TextHelper {

    private TextHelper() {}

    public static String capitalize(String s) {
        String[] parts = s.split(" ");
        StringBuilder sb = new StringBuilder();
        for(String word : parts) {
            sb.append(String.format("%s%s ", word.substring(0, 1).toUpperCase(), word.substring(1)));
        }
        return sb.toString().trim();
    }

    public static boolean isValidEmail(CharSequence email) {
        if(email == null) return false;
        return Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }
}
